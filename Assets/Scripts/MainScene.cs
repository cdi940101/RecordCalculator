﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class MainScene : MonoBehaviour
{
    public GameObject[] Hero;
    public GameObject[] User;
    public GameObject LeftWinButton;
    public GameObject RightWinButton;
    private string[] HeroName = null;
    private string[] UserName = null;
    private string WinLossText = null;

    public GameObject m_Input;
    public GameObject m_HeroRecordButton;


    void Start()
    {
        HeroName = new string[6];
        UserName = new string[6];

        for (int i = 0; i < 6; ++i)
        {
            HeroName[i] = "";
            UserName[i] = "";
        }


        LeftWinButton.GetComponent<Toggle>().isOn = true;

        //RightWinButton.GetComponent<Toggle>().isOn = true;

        //MatchStatistic.Instance.ShowAllHeroRecord();

        //User[0].transform.Find("Name").GetComponent<Placeholder>().
        //Hero[0].transform.Find("Name").GetComponent<Text>().text = "영웅";
        Hero[0].GetComponent<InputField>().Select();

        Screen.SetResolution(1600, 800, false);
        
    }

    // Update is called once per frame
    void Update()
    {

        //if (LeftWinButton.GetComponent<Toggle>().isOn)
        //{
        //    if (Input.GetKeyDown(KeyCode.Tab))
        //    {
        //        RightWinButton.GetComponent<Toggle>().isOn = true;
        //    }
        //}


        if (Input.GetKeyDown(KeyCode.F1) || Input.GetKeyDown(KeyCode.F4)) // save
        {
            SaveData();
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            LeftWinButton.GetComponent<Toggle>().isOn = true;
        }
        if (Input.GetKeyDown(KeyCode.F3))
        {
            RightWinButton.GetComponent<Toggle>().isOn = true;
        }
        

            for (int q = 0; q < 6; ++q)
        {
            if (Hero[q].GetComponent<InputField>().isFocused == true)
            {
                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    User[q].GetComponent<InputField>().Select();
                    
                }
            }
        }

        for (int w = 0; w < 6; ++w)
        {
            if (w == 5)
            {
                if (User[w].GetComponent<InputField>().isFocused == true)
                {
                    if (Input.GetKeyDown(KeyCode.Tab))
                    {
                        //Hero[0].GetComponent<InputField>().Select();
                        //WinLoss.GetComponent<InputField>().Select();
                        LeftWinButton.GetComponent<Toggle>().isOn = true;
                    }
                }

                break;
            }

            if (User[w].GetComponent<InputField>().isFocused == true)
            {
                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    Hero[w + 1].GetComponent<InputField>().Select();
                }
            }
        }



        //HeroName[0] = Hero[0].transform.Find("Name").GetComponent<Text>().text;
        //HeroName[1] = Hero[1].transform.Find("Name").GetComponent<Text>().text;
    }

    public void init()
    {
        for (int i = 0; i < 6; ++i)
        {
            //Hero[i].transform.Find("Name").GetComponent<Text>(). = "";
            User[i].transform.Find("Name").GetComponent<Text>().text = "";
        }
    }

    public void SaveData()
    {



        string Before = BeforeLoad();

        string Full = ReadData();

        for (int i = 0; i < 6; ++i)
        {
            if (HeroName[i] == "")
                return;
        }

        string last = Before + Full;

        // FileMode.Create는 덮어쓰기.
        FileStream f = new FileStream(Application.dataPath + "/StreamingAssets" + "/" + "RecordFile.txt", FileMode.Create, FileAccess.Write);


        StreamWriter writer = new StreamWriter(f, System.Text.Encoding.Unicode);
        writer.WriteLine(last);
        writer.Close();

        for (int i = 0; i < 6; ++i)
        {
            HeroName[i] = "";
            UserName[i] = "";
            Hero[i].GetComponent<InputField>().text = "";
            User[i].GetComponent<InputField>().text = "";

        }

        Hero[0].GetComponent<InputField>().Select();


    }
    string ReadData()
    {
        for (int e = 0; e < 6; ++e)
        {
            HeroName[e] = Hero[e].transform.Find("Name").GetComponent<Text>().text;
            UserName[e] = User[e].transform.Find("Name").GetComponent<Text>().text;
        }

        string Full = "";

        for (int i = 0; i < 6; ++i)
        {
            Full += HeroName[i] + "," + UserName[i] + ",";
        }

        if (LeftWinButton.GetComponent<Toggle>().isOn)
        {
            WinLossText = "왼";
        }
        else { WinLossText = "오"; }

        string date = WinLossText + "," + System.DateTime.Now.ToString("yyMMdd");

        Full += date;

        return Full;
    }
    public void LoadData()
    {

        StreamReader sr = new StreamReader(Application.dataPath + "/StreamingAssets" + "/" + "RecordFile.txt");

        string source = sr.ReadLine();

        string[] sptext = source.Split(',');
        for (int i = 0; i < sptext.Length; i++)
        {

        }

        sr.Close();
    }

    string BeforeLoad()
    {

        StreamReader sr = new StreamReader(Application.dataPath + "/StreamingAssets" + "/" + "RecordFile.txt");

        string beforetext = sr.ReadLine();


        sr.Close();

        beforetext += ",";
        return beforetext;

    }

    public void ChangeMode()
    {

        //m_Input.SetActive();
        //if (m_Input.activeSelf)
        //{
        //    m_Input.SetActive(false);
        //    m_Output.SetActive(true);
        //}
        //else
        //{
        //    m_Input.SetActive(true);
        //    m_Output.SetActive(false);
        //}

        if (m_Input.activeInHierarchy) // 부모영향받아 비활성화됐을때
        {

        }



    }
}


//항 목 형식문자열 설       명

//년   y    한 자리 연도이며, 2001은 "1"로 표시됩니다.
//     yy   연도의 마지막 두 자리이며, 2001은 "01"로 표시됩니다.
//     yyyy 완전한 형태의 연도이며, 2001은 "2001"로 표시됩니다.

//월   M    달을 나타내는 한 자리 또는 두 자리 숫자입니다.
//     MM   달을 나타내는 두 자리 숫자입니다. 한 자리로 된 값 앞에는 0이 옵니다.
//     MMM  세 문자로 된 달의 약어입니다.
//     MMMM 달의 전체 이름입니다.
//일   d    한 자리 또는 두 자리 날짜입니다.
//     dd   두 자리 날짜입니다. 한 자리로 된 날짜 값 앞에는 0이 옵니다.
//요일 ddd  세 문자로 된 요일 약어입니다.
//     dddd 요일의 전체 이름입니다.
//시간 h    12시간 형식의 한 자리 또는 두 자리 시간입니다.
//     hh   12시간 형식의 두 자리 시간입니다. 한 자리로 된 값 앞에는 0이 옵니다.
//     H    24시간 형식의 한 자리 또는 두 자리 시간입니다.
//     HH   24시간 형식의 두 자리 시간입니다. 한 자리로 된 값 앞에는 0이 옵니다.
//분   m    한 자리 또는 두 자리 분입니다.
//     mm   두 자리 분입니다. 한 자리로 된 값 앞에는 0이 옵니다.
//초   s    한 자리 또는 두 자리 초입니다.
//     ss   두 자리 초입니다. 한 자리로 된 값 앞에는 0이 옵니다.
//Am/pm t   한 문자로 된 A.M./P.M. 약어이며, A.M.은 "A"로 표시됩니다.
//      tt  두 문자로 된 A.M./P.M. 약어이며, A.M.은 "AM"으로 표시됩니다.

