﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

class HeroWL
{
    public string HeroName;
    public List<UserWL> listUserPlayingThisHero = new List<UserWL>();
    public int WinCount;
    public int LossCount;
    public List<DateWL> listDate = new List<DateWL>();
    public DateToDateWL DateToDateRecord = new DateToDateWL();
    public List<MateWL> listTeamMateHero = new List<MateWL>();
    public List<MateWL> listEnemyMateHero = new List<MateWL>();
}

class UserWL
{
    public string UserName;
    public List<HeroWL> listHeroPlayedbyUser = new List<HeroWL>();
    public int WinCount;
    public int LossCount;
    public List<DateWL> listDate = new List<DateWL>();
    public DateToDateWL DateToDateRecord = new DateToDateWL();
    public List<MateWL> listTeamMate = new List<MateWL>();
    public List<MateWL> listEnemyMate = new List<MateWL>();

}

class DateWL
{
    public string strDate;
    public int intDate;
    public int WinCount;
    public int LossCount;
    public int DateLeftWinCount;
    public int DateRightWinCount;
}

class MateWL
{
    public string Name;
    public int WinCount;
    public int LossCount;
}

class DateToDateWL
{
    public string Name;
    public string StartDate;
    public string EndDate;
    public int WinCount;
    public int LossCount;
    //public int DateLeftWinCount;
    //public int DateRightWinCount;
}




public class MatchStatistic : MonoBehaviour
{
    static public MatchStatistic Instance { private set; get; }

    public GameObject m_TextBoxCollection;

    //public GameObject m_FindingNameText;
    public GameObject m_MainHeroList;
    public GameObject m_DateHeroList;

    public GameObject m_FindingRecordList;
    public GameObject SampleHero = null;
    public GameObject SampleUser = null;

    List<HeroWL> listHeroWL = new List<HeroWL>();
    List<UserWL> listUserWL = new List<UserWL>();
    List<DateToDateWL> listDateToDateRecord = new List<DateToDateWL>();
    

    public GameObject m_AllMatchCount;
    public GameObject m_LeftWinRate;
    public GameObject m_RightWinRate;
    public GameObject m_UserWinRate;

    public GameObject m_LeftWinRateText;
    public GameObject m_RightWinRateText;
    public GameObject m_UserWinRateText;
    public GameObject m_LeftDateText;
    public GameObject m_RightDateText;
    //FindingNameText 왼쪽상단 제목텍스트
    string LastGameDate;

    public GameObject m_Input;
    public GameObject m_Output;
    public GameObject m_RealHeroRecords;

    private int iMatchCount = 1;
    private int iLeftWin = 0;
    private int iRightWin = 0;

    private List<GameObject> m_listInstanceHeroObj = new List<GameObject>();
    private List<GameObject> m_listInstanceUserObj = new List<GameObject>();
    private List<GameObject> m_listInstanceFindingObj = new List<GameObject>();

    private List<GameObject> m_listInstanceDateObj = new List<GameObject>();

    public GameObject m_SearchInputBox;
    public GameObject m_LeftDateInputBox;
    public GameObject m_RightDateInputBox;
    private bool FindingUserName = false;
    private bool FindingHeroName = false;
    string NowFindingName = "";

    private void Awake()
    {
        Instance = this;
    }

    void Update()
    {


        if (m_SearchInputBox.GetComponent<InputField>().isFocused == true)
        {

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (FindingUserName)
                    ClickedUserSearch();


                if (FindingHeroName)
                    ClickedHeroSearch();
            }
        }

        if (m_LeftDateInputBox.GetComponent<InputField>().isFocused == true)
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                m_RightDateInputBox.GetComponent<InputField>().Select();
            }
        }


    }

    private void LoadData()
    {
        listHeroWL.Clear();
        listUserWL.Clear();

        StreamReader sr = new StreamReader(Application.dataPath + "/StreamingAssets" + "/" + "RecordFile.txt");

        string source = sr.ReadLine();
        //경기당 14개 0 2 4 / 6 8 10 영웅
        //            1 3 5 / 7 9 11 유저

        string[] sptext = source.Split(',');
        int numcount = 0;
        string winorloss = "";
        int nownum = 0;

        for (int SplitIndex = 0; SplitIndex < sptext.Length; SplitIndex++)
        {
            bool bFirstHeroOverlapcheck = false;

            bool bFirstUserOverlapcheck = false;
            nownum++;
            winorloss = sptext[iMatchCount * 14 - 2];
            LastGameDate = sptext[iMatchCount * 14 - 1];
            if (numcount < 12)
            {
                if (SplitIndex % 2 == 0)// 영웅만
                {
                    for (int q = 0; q < listHeroWL.Count; ++q)
                    {
                        if (listHeroWL[q].HeroName == sptext[SplitIndex]) //만들어져있다면 포문정지
                        {
                            bFirstHeroOverlapcheck = true;
                            break;
                        }

                    }

                    if (!bFirstHeroOverlapcheck)
                    {
                        HeroWL mytemp = new HeroWL();

                        mytemp.HeroName = sptext[SplitIndex];
                        mytemp.WinCount = 0;
                        mytemp.LossCount = 0;

                        listHeroWL.Add(mytemp);
                    }




                    for (int w = 0; w < listHeroWL.Count; ++w)
                    {
                        if (listHeroWL[w].HeroName == sptext[SplitIndex]) // 특정 영웅의 승패 등록
                        {


                            #region 팀영웅과 적팀영웅 등록

                            string TeamMateHero1 = "";
                            string TeamMateHero2 = "";

                            string EnemyMateHero1 = "";
                            string EnemyMateHero2 = "";
                            string EnemyMateHero3 = "";

                            #region 팀영웅과 적팀영웅 이름등록




                            if (SplitIndex == iMatchCount * 14 - 14) // 자기자신찾기 0
                            {
                                TeamMateHero1 = sptext[iMatchCount * 14 - 12]; //2 2
                                TeamMateHero2 = sptext[iMatchCount * 14 - 10]; //3 4
                                EnemyMateHero1 = sptext[iMatchCount * 14 - 8]; //4 6 
                                EnemyMateHero2 = sptext[iMatchCount * 14 - 6]; //5 8
                                EnemyMateHero3 = sptext[iMatchCount * 14 - 4]; //6 10

                            }
                            else if (SplitIndex == iMatchCount * 14 - 12) // 4
                            {
                                TeamMateHero1 = sptext[iMatchCount * 14 - 14];
                                TeamMateHero2 = sptext[iMatchCount * 14 - 10];
                                EnemyMateHero1 = sptext[iMatchCount * 14 - 8]; //6 4 
                                EnemyMateHero2 = sptext[iMatchCount * 14 - 6]; //8 5
                                EnemyMateHero3 = sptext[iMatchCount * 14 - 4]; //10 6
                            }
                            else if (SplitIndex == iMatchCount * 14 - 10) // 6
                            {
                                TeamMateHero1 = sptext[iMatchCount * 14 - 14];
                                TeamMateHero2 = sptext[iMatchCount * 14 - 12];
                                EnemyMateHero1 = sptext[iMatchCount * 14 - 8]; //6 4 
                                EnemyMateHero2 = sptext[iMatchCount * 14 - 6]; //8 5
                                EnemyMateHero3 = sptext[iMatchCount * 14 - 4]; //10 6
                            }
                            else if (SplitIndex == iMatchCount * 14 - 8) // 8
                            {
                                TeamMateHero1 = sptext[iMatchCount * 14 - 6];
                                TeamMateHero2 = sptext[iMatchCount * 14 - 4];
                                EnemyMateHero1 = sptext[iMatchCount * 14 - 14]; //0 1 
                                EnemyMateHero2 = sptext[iMatchCount * 14 - 12]; //2 2
                                EnemyMateHero3 = sptext[iMatchCount * 14 - 10]; //4 3
                            }
                            else if (SplitIndex == iMatchCount * 14 - 6) // 10
                            {
                                TeamMateHero1 = sptext[iMatchCount * 14 - 8];
                                TeamMateHero2 = sptext[iMatchCount * 14 - 4];
                                EnemyMateHero1 = sptext[iMatchCount * 14 - 14]; //0 1 
                                EnemyMateHero2 = sptext[iMatchCount * 14 - 12]; //2 2
                                EnemyMateHero3 = sptext[iMatchCount * 14 - 10]; //4 3
                            }
                            else if (SplitIndex == iMatchCount * 14 - 4) // 12
                            {
                                TeamMateHero1 = sptext[iMatchCount * 14 - 8];
                                TeamMateHero2 = sptext[iMatchCount * 14 - 6];
                                EnemyMateHero1 = sptext[iMatchCount * 14 - 14]; //0 1 
                                EnemyMateHero2 = sptext[iMatchCount * 14 - 12]; //2 2
                                EnemyMateHero3 = sptext[iMatchCount * 14 - 10]; //4 3
                            }


                            #endregion

                           

                            RegisterTeamMateHero(TeamMateHero1, TeamMateHero2, listHeroWL[w], SplitIndex, sptext);
                            RegisterEnemyMateHero(EnemyMateHero1, EnemyMateHero2, EnemyMateHero3, listHeroWL[w], SplitIndex, sptext);


                            #endregion




                            AddHeroDateList(listHeroWL[w], sptext[iMatchCount * 14 - 1]);

                            if (winorloss == "오") // 우측팀승
                            {
                                if (numcount > 5) //현재 케릭이 우측팀이라면
                                {
                                    listHeroWL[w].WinCount++;
                                    AddlistHeroWLDateWinLoss(listHeroWL[w], true, sptext[iMatchCount * 14 - 1], "오");
                                    RegisterTeamMateWLCount(TeamMateHero1, TeamMateHero2, listHeroWL[w].listTeamMateHero, true);
                                    RegisterEnemyMateWLCount(EnemyMateHero1, EnemyMateHero2, EnemyMateHero3, listHeroWL[w].listEnemyMateHero, true);
                                }
                                else
                                {
                                    listHeroWL[w].LossCount++;
                                    AddlistHeroWLDateWinLoss(listHeroWL[w], false, sptext[iMatchCount * 14 - 1], "오");
                                    RegisterTeamMateWLCount(TeamMateHero1, TeamMateHero2, listHeroWL[w].listTeamMateHero, false);
                                    RegisterEnemyMateWLCount(EnemyMateHero1, EnemyMateHero2, EnemyMateHero3, listHeroWL[w].listEnemyMateHero, false);

                                }
                            }
                            else
                            {
                                if (numcount > 5)
                                {
                                    listHeroWL[w].LossCount++;
                                    AddlistHeroWLDateWinLoss(listHeroWL[w], false, sptext[iMatchCount * 14 - 1], "왼");
                                    RegisterTeamMateWLCount(TeamMateHero1, TeamMateHero2, listHeroWL[w].listTeamMateHero, false);
                                    RegisterEnemyMateWLCount(EnemyMateHero1, EnemyMateHero2, EnemyMateHero3, listHeroWL[w].listEnemyMateHero, false);
                                }
                                else
                                {
                                    listHeroWL[w].WinCount++;
                                    AddlistHeroWLDateWinLoss(listHeroWL[w], true, sptext[iMatchCount * 14 - 1], "왼");
                                    RegisterTeamMateWLCount(TeamMateHero1, TeamMateHero2, listHeroWL[w].listTeamMateHero, true);
                                    RegisterEnemyMateWLCount(EnemyMateHero1, EnemyMateHero2, EnemyMateHero3, listHeroWL[w].listEnemyMateHero, true);
                                }

                            }




                            bool bUserOverlapCheck = false; // 이 영웅을 플레이했던 유저등록

                            for (int r = 0; r < listHeroWL[w].listUserPlayingThisHero.Count; ++r) // 유저의 영웅 중복 검색
                            {
                                if (listHeroWL[w].listUserPlayingThisHero[r].UserName == sptext[SplitIndex + 1])
                                {
                                    bUserOverlapCheck = true;
                                    break;
                                }

                            }

                            if (!bUserOverlapCheck)
                            {
                                UserWL hwltemp = new UserWL();
                                hwltemp.UserName = sptext[SplitIndex + 1];
                                hwltemp.WinCount = 0;
                                hwltemp.LossCount = 0;
                                listHeroWL[w].listUserPlayingThisHero.Add(hwltemp);
                            }

                            for (int r = 0; r < listHeroWL[w].listUserPlayingThisHero.Count; ++r) // 이영웅을 플레이했던 유저들의 승패 등록
                            {
                                if (listHeroWL[w].listUserPlayingThisHero[r].UserName == sptext[SplitIndex + 1])
                                {
                                    //UserWL
                                    AddUserDateList(listHeroWL[w].listUserPlayingThisHero[r], sptext[iMatchCount * 14 - 1]);

                                    if (winorloss == "오") // 우측팀승
                                    {
                                        if (numcount > 5) // 현재 케릭이 우측팀이라면
                                        {
                                            listHeroWL[w].listUserPlayingThisHero[r].WinCount++;
                                            AddlistUserWLDateWinLoss(listHeroWL[w].listUserPlayingThisHero[r], true, sptext[iMatchCount * 14 - 1], "오");


                                        }
                                        else
                                        {
                                            listHeroWL[w].listUserPlayingThisHero[r].LossCount++;
                                            AddlistUserWLDateWinLoss(listHeroWL[w].listUserPlayingThisHero[r], false, sptext[iMatchCount * 14 - 1], "오");


                                        }
                                    }
                                    else
                                    {
                                        if (numcount > 5) // 현재 케릭이 우측팀이라면
                                        {
                                            listHeroWL[w].listUserPlayingThisHero[r].LossCount++;
                                            AddlistUserWLDateWinLoss(listHeroWL[w].listUserPlayingThisHero[r], false, sptext[iMatchCount * 14 - 1], "왼");
                                        }
                                        else
                                        {
                                            listHeroWL[w].listUserPlayingThisHero[r].WinCount++;
                                            AddlistUserWLDateWinLoss(listHeroWL[w].listUserPlayingThisHero[r], true, sptext[iMatchCount * 14 - 1], "왼");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (SplitIndex % 2 == 1) // 유저만
                {
                    for (int q = 0; q < listUserWL.Count; ++q)
                    {
                        if (listUserWL[q].UserName == sptext[SplitIndex]) // 중복검사
                        {
                            bFirstUserOverlapcheck = true;
                            break;
                        }
                    }

                    if (!bFirstUserOverlapcheck) // 미등록시 리스트에 등록
                    {
                        UserWL Utemp = new UserWL();

                        Utemp.UserName = sptext[SplitIndex];
                        Utemp.WinCount = 0;
                        Utemp.LossCount = 0;

                        listUserWL.Add(Utemp);
                        //listHeroName.Add(sptext[i]);
                    }

                    for (int w = 0; w < listUserWL.Count; ++w) // 전체 탐색
                    {

                        if (listUserWL[w].UserName == sptext[SplitIndex]) // 특정유저의승패등록
                        {

                            #region 팀원과 적팀원 등록

                            string TeamMate1 = "";
                            string TeamMate2 = "";

                            string EnemyMate1 = "";
                            string EnemyMate2 = "";
                            string EnemyMate3 = "";

                            #region 팀원과 적팀원 이름등록




                            if (SplitIndex == iMatchCount * 14 - 13) // 자기자신찾기 1
                            {
                                TeamMate1 = sptext[iMatchCount * 14 - 11]; //2 3
                                TeamMate2 = sptext[iMatchCount * 14 - 9]; //3 5
                                EnemyMate1 = sptext[iMatchCount * 14 - 7]; //4 7 
                                EnemyMate2 = sptext[iMatchCount * 14 - 5]; //5 9
                                EnemyMate3 = sptext[iMatchCount * 14 - 3]; //6 11

                            }
                            else if (SplitIndex == iMatchCount * 14 - 11) // 3
                            {
                                TeamMate1 = sptext[iMatchCount * 14 - 13];
                                TeamMate2 = sptext[iMatchCount * 14 - 9];
                                EnemyMate1 = sptext[iMatchCount * 14 - 7]; //4 7 
                                EnemyMate2 = sptext[iMatchCount * 14 - 5]; //5 9
                                EnemyMate3 = sptext[iMatchCount * 14 - 3]; //6 11
                            }
                            else if (SplitIndex == iMatchCount * 14 - 9) // 5
                            {
                                TeamMate1 = sptext[iMatchCount * 14 - 13];
                                TeamMate2 = sptext[iMatchCount * 14 - 11];
                                EnemyMate1 = sptext[iMatchCount * 14 - 7]; //4 7 
                                EnemyMate2 = sptext[iMatchCount * 14 - 5]; //5 9
                                EnemyMate3 = sptext[iMatchCount * 14 - 3]; //6 11
                            }
                            else if (SplitIndex == iMatchCount * 14 - 7) // 7
                            {
                                TeamMate1 = sptext[iMatchCount * 14 - 5];
                                TeamMate2 = sptext[iMatchCount * 14 - 3];
                                EnemyMate1 = sptext[iMatchCount * 14 - 13]; //1 1 
                                EnemyMate2 = sptext[iMatchCount * 14 - 11]; //2 3
                                EnemyMate3 = sptext[iMatchCount * 14 - 9]; //3 5
                            }
                            else if (SplitIndex == iMatchCount * 14 - 5) // 9
                            {
                                TeamMate1 = sptext[iMatchCount * 14 - 7];
                                TeamMate2 = sptext[iMatchCount * 14 - 3];
                                EnemyMate1 = sptext[iMatchCount * 14 - 13]; //1 1 
                                EnemyMate2 = sptext[iMatchCount * 14 - 11]; //2 3
                                EnemyMate3 = sptext[iMatchCount * 14 - 9]; //3 5
                            }
                            else if (SplitIndex == iMatchCount * 14 - 3) // 11
                            {
                                TeamMate1 = sptext[iMatchCount * 14 - 7];
                                TeamMate2 = sptext[iMatchCount * 14 - 5];
                                EnemyMate1 = sptext[iMatchCount * 14 - 13]; //1 1 
                                EnemyMate2 = sptext[iMatchCount * 14 - 11]; //2 3
                                EnemyMate3 = sptext[iMatchCount * 14 - 9]; //3 5
                            }


                            #endregion

                            RegisterTeamMate(TeamMate1, TeamMate2, listUserWL[w], SplitIndex, sptext);
                            RegisterEnemyMate(EnemyMate1, EnemyMate2, EnemyMate3, listUserWL[w], SplitIndex, sptext);


                            #endregion


                            AddUserDateList(listUserWL[w], sptext[iMatchCount * 14 - 1]);


                            if (winorloss == "오") // 우측팀승
                            {
                                if (numcount > 5) // 해당유저가 우측팀
                                {
                                    listUserWL[w].WinCount++;
                                    AddlistUserWLDateWinLoss(listUserWL[w], true, sptext[iMatchCount * 14 - 1], "오");

                                    RegisterTeamMateWLCount(TeamMate1, TeamMate2, listUserWL[w].listTeamMate, true);
                                    RegisterEnemyMateWLCount(EnemyMate1, EnemyMate2, EnemyMate3, listUserWL[w].listEnemyMate, true);


                                }
                                else
                                {
                                    listUserWL[w].LossCount++;
                                    AddlistUserWLDateWinLoss(listUserWL[w], false, sptext[iMatchCount * 14 - 1], "오");
                                    RegisterTeamMateWLCount(TeamMate1, TeamMate2, listUserWL[w].listTeamMate, false);
                                    RegisterEnemyMateWLCount(EnemyMate1, EnemyMate2, EnemyMate3, listUserWL[w].listEnemyMate, false);


                                }
                            }
                            else
                            {
                                if (numcount > 5) // 해당유저가 우측팀
                                {
                                    listUserWL[w].LossCount++;
                                    AddlistUserWLDateWinLoss(listUserWL[w], false, sptext[iMatchCount * 14 - 1], "왼");
                                    RegisterTeamMateWLCount(TeamMate1, TeamMate2, listUserWL[w].listTeamMate, false);
                                    RegisterEnemyMateWLCount(EnemyMate1, EnemyMate2, EnemyMate3, listUserWL[w].listEnemyMate, false);
                                }
                                else
                                {
                                    listUserWL[w].WinCount++;
                                    AddlistUserWLDateWinLoss(listUserWL[w], true, sptext[iMatchCount * 14 - 1], "왼");
                                    RegisterTeamMateWLCount(TeamMate1, TeamMate2, listUserWL[w].listTeamMate, true);
                                    RegisterEnemyMateWLCount(EnemyMate1, EnemyMate2, EnemyMate3, listUserWL[w].listEnemyMate, true);
                                }

                            }



                            bool bHeroOverlapCheck = false; // 유저가 플레이했던 영웅등록

                            for (int r = 0; r < listUserWL[w].listHeroPlayedbyUser.Count; ++r) // 유저의 영웅 중복 검색
                            {
                                if (listUserWL[w].listHeroPlayedbyUser[r].HeroName == sptext[SplitIndex - 1])
                                {
                                    bHeroOverlapCheck = true;
                                    break;
                                }

                            }

                            if (!bHeroOverlapCheck)
                            {
                                HeroWL hwltemp = new HeroWL();
                                hwltemp.HeroName = sptext[SplitIndex - 1];
                                hwltemp.WinCount = 0;
                                hwltemp.LossCount = 0;
                                listUserWL[w].listHeroPlayedbyUser.Add(hwltemp);
                            }


                            for (int r = 0; r < listUserWL[w].listHeroPlayedbyUser.Count; ++r) // 유저의 영웅 중복 검색
                            {

                                if (listUserWL[w].listHeroPlayedbyUser[r].HeroName == sptext[SplitIndex - 1])
                                {
                                    AddHeroDateList(listUserWL[w].listHeroPlayedbyUser[r], sptext[iMatchCount * 14 - 1]);

                                    if (winorloss == "오") // 우측팀승
                                    {
                                        if (numcount > 5) // 현재 케릭이 우측팀이라면
                                        {
                                            listUserWL[w].listHeroPlayedbyUser[r].WinCount++;

                                            AddlistHeroWLDateWinLoss(listUserWL[w].listHeroPlayedbyUser[r], true, sptext[iMatchCount * 14 - 1], "오");
                                        }
                                        else
                                        {
                                            listUserWL[w].listHeroPlayedbyUser[r].LossCount++;
                                            AddlistHeroWLDateWinLoss(listUserWL[w].listHeroPlayedbyUser[r], false, sptext[iMatchCount * 14 - 1], "오");

                                        }
                                    }
                                    else
                                    {
                                        if (numcount > 5) // 현재 케릭이 우측팀이라면
                                        {
                                            listUserWL[w].listHeroPlayedbyUser[r].LossCount++;
                                            AddlistHeroWLDateWinLoss(listUserWL[w].listHeroPlayedbyUser[r], false, sptext[iMatchCount * 14 - 1], "왼");

                                        }
                                        else
                                        {
                                            listUserWL[w].listHeroPlayedbyUser[r].WinCount++;
                                            AddlistHeroWLDateWinLoss(listUserWL[w].listHeroPlayedbyUser[r], true, sptext[iMatchCount * 14 - 1], "왼");


                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            numcount++;
            if (numcount % 14 == 0)
            {
                if (winorloss == "오")
                {
                    iRightWin++;

                }
                else
                {
                    iLeftWin++;

                }

                numcount = 0; // 1경기끝
                iMatchCount++;
            }
        }

        iMatchCount--; // 1개 더 더해진거 뺌
        sr.Close();



        float awinrate = 0;
        float bwinrate = 0;
        int aheromatchcount = 0;
        int bheromatchcount = 0;

        listHeroWL.Sort(
        delegate (HeroWL a, HeroWL b)
        {
            aheromatchcount = a.WinCount + a.LossCount;
            bheromatchcount = b.WinCount + b.LossCount;
            awinrate = (float)a.WinCount / (float)aheromatchcount;
            bwinrate = (float)b.WinCount / (float)bheromatchcount;

            if (awinrate < bwinrate) return 1;
            else if (awinrate > bwinrate) return -1;
            return 0;
        });

        for (int y = 0; y < listHeroWL.Count; ++y)
        {
            listHeroWL[y].listTeamMateHero.Sort(
          delegate (MateWL a, MateWL b)
          {
              aheromatchcount = a.WinCount + a.LossCount;
              bheromatchcount = b.WinCount + b.LossCount;
              awinrate = (float)a.WinCount / (float)aheromatchcount;
              bwinrate = (float)b.WinCount / (float)bheromatchcount;

              if (awinrate < bwinrate) return 1;
              else if (awinrate > bwinrate) return -1;
              return 0;
          });
        }

        for (int y = 0; y < listHeroWL.Count; ++y)
        {
            listHeroWL[y].listEnemyMateHero.Sort( // 오름차순
          delegate (MateWL a, MateWL b)
          {
              aheromatchcount = a.WinCount + a.LossCount;
              bheromatchcount = b.WinCount + b.LossCount;
              awinrate = (float)a.WinCount / (float)aheromatchcount;
              bwinrate = (float)b.WinCount / (float)bheromatchcount;

              if (awinrate < bwinrate) return -1;
              else if (awinrate > bwinrate) return 1;
              return 0;
          });
        }



        listUserWL.Sort( //유저 승률별 정렬
        delegate (UserWL a, UserWL b)
        {
            aheromatchcount = a.WinCount + a.LossCount;
            bheromatchcount = b.WinCount + b.LossCount;
            awinrate = (float)a.WinCount / (float)aheromatchcount;
            bwinrate = (float)b.WinCount / (float)bheromatchcount;

            if (awinrate < bwinrate) return 1;
            else if (awinrate > bwinrate) return -1;
            return 0;
        });

        for (int y = 0; y < listUserWL.Count; ++y)
        {
            listUserWL[y].listHeroPlayedbyUser.Sort(
          delegate (HeroWL a, HeroWL b)
          {
              aheromatchcount = a.WinCount + a.LossCount;
              bheromatchcount = b.WinCount + b.LossCount;
              awinrate = (float)a.WinCount / (float)aheromatchcount;
              bwinrate = (float)b.WinCount / (float)bheromatchcount;

              if (awinrate < bwinrate) return 1;
              else if (awinrate > bwinrate) return -1;
              return 0;
          });
        }


        for (int y = 0; y < listUserWL.Count; ++y)
        {
            listUserWL[y].listTeamMate.Sort(
          delegate (MateWL a, MateWL b)
          {
              aheromatchcount = a.WinCount + a.LossCount;
              bheromatchcount = b.WinCount + b.LossCount;
              awinrate = (float)a.WinCount / (float)aheromatchcount;
              bwinrate = (float)b.WinCount / (float)bheromatchcount;

              if (awinrate < bwinrate) return 1;
              else if (awinrate > bwinrate) return -1;
              return 0;
          });
        }

        for (int y = 0; y < listUserWL.Count; ++y)
        {
            listUserWL[y].listEnemyMate.Sort( // 오름차순
          delegate (MateWL a, MateWL b)
          {
              aheromatchcount = a.WinCount + a.LossCount;
              bheromatchcount = b.WinCount + b.LossCount;
              awinrate = (float)a.WinCount / (float)aheromatchcount;
              bwinrate = (float)b.WinCount / (float)bheromatchcount;

              if (awinrate < bwinrate) return -1;
              else if (awinrate > bwinrate) return 1;
              return 0;
          });
        }

    }
    public void ShowAllHeroRecord() // 전체 영웅
    {

        if (m_Input.activeSelf)
        {
            m_Input.SetActive(false);
            m_Output.SetActive(true);
        }
        else
        {
            if (m_listInstanceFindingObj.Count == 0 && m_listInstanceUserObj.Count == 0 &&
                m_listInstanceDateObj.Count == 0) // 수정하자!
            {
                m_Input.SetActive(true);
                m_Output.SetActive(false);
                return;
            }
        }

        for (int t = 0; t < m_listInstanceHeroObj.Count; ++t)
        {
            Destroy(m_listInstanceHeroObj[t]);
        }
        m_listInstanceHeroObj.Clear();

        foreach (var obj in m_listInstanceDateObj)
            Destroy(obj);
        m_listInstanceDateObj.Clear();

        foreach (var obj in m_listInstanceFindingObj)
            Destroy(obj);
        m_listInstanceFindingObj.Clear();



        iMatchCount = 1;
        iRightWin = 0;
        iLeftWin = 0;

        LoadData();


        for (int i = 0; i < listHeroWL.Count; ++i)
        {
            GameObject Instance = (GameObject)Instantiate(SampleHero, m_MainHeroList.transform);

            Instance.SetActive(true);

            int matchcount = listHeroWL[i].WinCount + listHeroWL[i].LossCount;


            float floatnum = (float)listHeroWL[i].WinCount / (float)matchcount;
            double rate = Math.Truncate(floatnum * 1000) / 1000 * 100;

            Instance.transform.Find("HeroName").GetComponent<Text>().text = listHeroWL[i].HeroName;
            Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
            Instance.transform.Find("Win").GetComponent<Text>().text = listHeroWL[i].WinCount.ToString();
            Instance.transform.Find("Loss").GetComponent<Text>().text = listHeroWL[i].LossCount.ToString();
            Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";
            m_listInstanceHeroObj.Add(Instance);

        }

        float lwr = (float)iLeftWin / (float)iMatchCount;
        double lrate = ((Math.Truncate(lwr * 1000) / 1000) * 100);

        float rwr = (float)iRightWin / (float)iMatchCount;
        double rrate = (Math.Truncate(rwr * 1000) / 1000) * 100;

        m_AllMatchCount.GetComponent<Text>().text = iMatchCount.ToString();
        m_LeftWinRate.GetComponent<Text>().text = lrate.ToString() + "%";
        m_RightWinRate.GetComponent<Text>().text = rrate.ToString() + "%";


        //기본세팅
        m_LeftWinRateText.GetComponent<Text>().text = "왼쪽팀승률 :";
        m_RightWinRateText.GetComponent<Text>().text = "우측팀승률 :";
        m_UserWinRateText.GetComponent<Text>().text = "";
        m_UserWinRate.GetComponent<Text>().text = "";

        m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = "전체 영웅 승률";
        m_SearchInputBox.GetComponent<InputField>().text = "";
        m_TextBoxCollection.transform.Find("HeroTextList0").transform.Find("HeroText").GetComponent<Text>().text = "영웅";
        m_TextBoxCollection.transform.Find("HeroTextList1").transform.Find("HeroText").GetComponent<Text>().text = "영웅";
        m_TextBoxCollection.transform.Find("HeroTextList2").transform.Find("HeroText").GetComponent<Text>().text = "영웅";

        m_LeftDateText.GetComponent<Text>().text = "210126"; // 최초 기록 시작한 날짜
        m_RightDateText.GetComponent<Text>().text = LastGameDate;
    }

    public void ShowAllUserWinRate() //전체 유저 승률
    {
        if (m_listInstanceUserObj.Count != 0)
        {
            ShowAllHeroRecord();

            for (int y = 0; y < m_listInstanceUserObj.Count; ++y)
                Destroy(m_listInstanceUserObj[y]);
            m_listInstanceUserObj.Clear();


            return;
        }

        foreach (var obj in m_listInstanceDateObj)
            Destroy(obj);
        m_listInstanceDateObj.Clear();

        foreach (var obj in m_listInstanceHeroObj)
        {
            Destroy(obj);
        }
        m_listInstanceHeroObj.Clear();

        foreach (var obj in m_listInstanceFindingObj)
        {
            Destroy(obj);
        }
        m_listInstanceFindingObj.Clear();




        for (int i = 0; i < listUserWL.Count; ++i)
        {
            GameObject Instance = (GameObject)Instantiate(SampleHero, m_MainHeroList.transform);

            Instance.SetActive(true);

            int matchcount = listUserWL[i].WinCount + listUserWL[i].LossCount;

            float floatnum = (float)listUserWL[i].WinCount / (float)matchcount;
            double rate = ((Math.Truncate(floatnum * 1000) / 1000) * 100);

            Instance.transform.Find("HeroName").GetComponent<Text>().text = listUserWL[i].UserName;
            Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
            Instance.transform.Find("Win").GetComponent<Text>().text = listUserWL[i].WinCount.ToString();
            Instance.transform.Find("Loss").GetComponent<Text>().text = listUserWL[i].LossCount.ToString();
            Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";
            m_listInstanceUserObj.Add(Instance);

        }

        float lwr = (float)iLeftWin / (float)iMatchCount;
        double lrate = ((Math.Truncate(lwr * 1000) / 1000) * 100);

        float rwr = (float)iRightWin / (float)iMatchCount;
        double rrate = ((Math.Truncate(rwr * 1000) / 1000) * 100);

        m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = "전체 유저 승률";
        m_AllMatchCount.GetComponent<Text>().text = iMatchCount.ToString();
        m_LeftWinRate.GetComponent<Text>().text = lrate.ToString() + "%";
        m_RightWinRate.GetComponent<Text>().text = rrate.ToString() + "%";

        m_TextBoxCollection.transform.Find("HeroTextList0").transform.Find("HeroText").GetComponent<Text>().text = "유저";
        m_TextBoxCollection.transform.Find("HeroTextList1").transform.Find("HeroText").GetComponent<Text>().text = "유저";
        m_TextBoxCollection.transform.Find("HeroTextList2").transform.Find("HeroText").GetComponent<Text>().text = "유저";


    }

    public void ClickedUserSearch() // 특정 유저 검색 버튼
    {
        if (FindingHeroName)
            FindingHeroName = false;

        if (!FindingUserName)
            FindingUserName = true;

        if (!m_SearchInputBox.activeSelf)
        {
            if (m_listInstanceFindingObj.Count == 0)
            {
                m_SearchInputBox.SetActive(true);
                m_SearchInputBox.GetComponent<InputField>().Select();
            }
            else
            {
                ShowAllHeroRecord();

                for (int t = 0; t < m_listInstanceFindingObj.Count; ++t)
                {
                    Destroy(m_listInstanceFindingObj[t]);
                }
                m_listInstanceFindingObj.Clear();

                foreach (var obj in m_listInstanceDateObj)
                    Destroy(obj);
                m_listInstanceDateObj.Clear();


                float lwr = (float)iLeftWin / (float)iMatchCount;
                double lrate = ((Math.Truncate(lwr * 1000) / 1000) * 100);

                float rwr = (float)iRightWin / (float)iMatchCount;
                double rrate = ((Math.Truncate(rwr * 1000) / 1000) * 100);

                m_AllMatchCount.GetComponent<Text>().text = iMatchCount.ToString();
                m_LeftWinRate.GetComponent<Text>().text = lrate.ToString() + "%";
                m_RightWinRate.GetComponent<Text>().text = rrate.ToString() + "%";


            }
        }
        else
        {
            if (m_SearchInputBox.transform.Find("Name").GetComponent<Text>().text != "")
            {
                m_SearchInputBox.SetActive(false);
                ShowUserRecord();
            }
            else
            {
                m_SearchInputBox.SetActive(false);
                m_SearchInputBox.GetComponent<InputField>().text = "";
            }
        }

    }

    void ShowUserRecord() // 특정 유저 검색
    {

        for (int q = 0; q < listUserWL.Count; ++q)
        {
            if (m_SearchInputBox.GetComponent<InputField>().text == listUserWL[q].UserName)
            {
                foreach (var obj in m_listInstanceHeroObj)
                    Destroy(obj);
                m_listInstanceHeroObj.Clear();

                foreach (var obj in m_listInstanceDateObj)
                    Destroy(obj);
                m_listInstanceDateObj.Clear();

                foreach (var obj in m_listInstanceUserObj)
                    Destroy(obj);
                m_listInstanceUserObj.Clear();



                NowFindingName = listUserWL[q].UserName;
                m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = NowFindingName + "의 영웅 전적";

                for (int i = 0; i < listUserWL[q].listHeroPlayedbyUser.Count; ++i)
                {
                    GameObject Instance = (GameObject)Instantiate(SampleHero, m_FindingRecordList.transform);

                    Instance.SetActive(true);

                    int matchcount = listUserWL[q].listHeroPlayedbyUser[i].WinCount + listUserWL[q].listHeroPlayedbyUser[i].LossCount;


                    float floatnum = (float)listUserWL[q].listHeroPlayedbyUser[i].WinCount / (float)matchcount;
                    double rate = ((Math.Truncate(floatnum * 1000) / 1000) * 100);

                    Instance.transform.Find("HeroName").GetComponent<Text>().text = listUserWL[q].listHeroPlayedbyUser[i].HeroName;
                    Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
                    Instance.transform.Find("Win").GetComponent<Text>().text = listUserWL[q].listHeroPlayedbyUser[i].WinCount.ToString();
                    Instance.transform.Find("Loss").GetComponent<Text>().text = listUserWL[q].listHeroPlayedbyUser[i].LossCount.ToString();
                    Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";
                    m_listInstanceFindingObj.Add(Instance);

                }


                int UserMatchCount = listUserWL[q].WinCount + listUserWL[q].LossCount;

                float uwr = (float)listUserWL[q].WinCount / (float)UserMatchCount;
                double urate = ((Math.Truncate(uwr * 1000) / 1000) * 100);


                m_AllMatchCount.GetComponent<Text>().text = UserMatchCount.ToString();
                m_LeftWinRate.GetComponent<Text>().text = listUserWL[q].WinCount.ToString(); // 승
                m_RightWinRate.GetComponent<Text>().text = listUserWL[q].LossCount.ToString(); // 패
                m_UserWinRate.GetComponent<Text>().text = urate.ToString() + "%";

                m_LeftWinRateText.GetComponent<Text>().text = "승리 :";
                m_RightWinRateText.GetComponent<Text>().text = "패배 :";
                m_UserWinRateText.GetComponent<Text>().text = "승률 :";

                m_TextBoxCollection.transform.Find("HeroTextList0").transform.Find("HeroText").GetComponent<Text>().text = "유저";
                m_TextBoxCollection.transform.Find("HeroTextList1").transform.Find("HeroText").GetComponent<Text>().text = "유저";
                m_TextBoxCollection.transform.Find("HeroTextList2").transform.Find("HeroText").GetComponent<Text>().text = "유저";
                break;
            }
        }
    }

    public void ClickedHeroSearch() // 특정 영웅 검색 버튼
    {
        if (!FindingHeroName)
            FindingHeroName = true;

        if (FindingUserName)
            FindingUserName = false;

        if (!m_SearchInputBox.activeSelf)
        {
            if (m_listInstanceFindingObj.Count == 0)
            {
                m_SearchInputBox.SetActive(true);
                m_SearchInputBox.GetComponent<InputField>().Select();

            }
            else
            {
                ShowAllHeroRecord();

                for (int t = 0; t < m_listInstanceFindingObj.Count; ++t)
                {
                    Destroy(m_listInstanceFindingObj[t]);
                }
                m_listInstanceFindingObj.Clear();

                foreach (var obj in m_listInstanceDateObj)
                    Destroy(obj);
                m_listInstanceDateObj.Clear();


                float lwr = (float)iLeftWin / (float)iMatchCount;
                double lrate = ((Math.Truncate(lwr * 1000) / 1000) * 100);

                float rwr = (float)iRightWin / (float)iMatchCount;
                double rrate = ((Math.Truncate(rwr * 1000) / 1000) * 100);

                m_AllMatchCount.GetComponent<Text>().text = iMatchCount.ToString();
                m_LeftWinRate.GetComponent<Text>().text = lrate.ToString() + "%";
                m_RightWinRate.GetComponent<Text>().text = rrate.ToString() + "%";



            }
        }
        else
        {
            if (m_SearchInputBox.transform.Find("Name").GetComponent<Text>().text != "")
            {
                m_SearchInputBox.SetActive(false);
                ShowHeroRecord();
            }
            else
            {
                m_SearchInputBox.SetActive(false);
                m_SearchInputBox.GetComponent<InputField>().text = "";

            }
        }

    }


    void ShowHeroRecord() // 특정 영웅 검색
    {

        for (int q = 0; q < listHeroWL.Count; ++q)
        {
            if (m_SearchInputBox.GetComponent<InputField>().text == listHeroWL[q].HeroName)
            {
                foreach (var obj in m_listInstanceHeroObj)
                    Destroy(obj);
                m_listInstanceHeroObj.Clear();

                foreach (var obj in m_listInstanceDateObj)
                    Destroy(obj);
                m_listInstanceDateObj.Clear();

                foreach (var obj in m_listInstanceUserObj)
                    Destroy(obj);
                m_listInstanceUserObj.Clear();

                NowFindingName = listHeroWL[q].HeroName;

                m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = listHeroWL[q].HeroName + "의 유저별 전적"; ;


                for (int i = 0; i < listHeroWL[q].listUserPlayingThisHero.Count; ++i)
                {
                    GameObject Instance = (GameObject)Instantiate(SampleHero, m_FindingRecordList.transform);

                    Instance.SetActive(true);

                    int matchcount = listHeroWL[q].listUserPlayingThisHero[i].WinCount + listHeroWL[q].listUserPlayingThisHero[i].LossCount;


                    float floatnum = (float)listHeroWL[q].listUserPlayingThisHero[i].WinCount / (float)matchcount;
                    double rate = ((Math.Truncate(floatnum * 1000) / 1000) * 100);

                    Instance.transform.Find("HeroName").GetComponent<Text>().text = listHeroWL[q].listUserPlayingThisHero[i].UserName;
                    Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
                    Instance.transform.Find("Win").GetComponent<Text>().text = listHeroWL[q].listUserPlayingThisHero[i].WinCount.ToString();
                    Instance.transform.Find("Loss").GetComponent<Text>().text = listHeroWL[q].listUserPlayingThisHero[i].LossCount.ToString();
                    Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";

                    m_listInstanceFindingObj.Add(Instance);
                }
                for (int y = 0; y < m_listInstanceHeroObj.Count; ++y)
                    Destroy(m_listInstanceHeroObj[y]);
                m_listInstanceHeroObj.Clear();

                int FindingNameMatchCount = listHeroWL[q].WinCount + listHeroWL[q].LossCount;

                float uwr = (float)listHeroWL[q].WinCount / (float)FindingNameMatchCount;
                double urate = ((Math.Truncate(uwr * 1000) / 1000) * 100);


                m_AllMatchCount.GetComponent<Text>().text = FindingNameMatchCount.ToString();
                m_LeftWinRate.GetComponent<Text>().text = listHeroWL[q].WinCount.ToString(); // 승
                m_RightWinRate.GetComponent<Text>().text = listHeroWL[q].LossCount.ToString(); // 패
                m_UserWinRate.GetComponent<Text>().text = urate.ToString() + "%";



                m_LeftWinRateText.GetComponent<Text>().text = "승리 :";
                m_RightWinRateText.GetComponent<Text>().text = "패배 :";
                m_UserWinRateText.GetComponent<Text>().text = "승률 :";

                m_TextBoxCollection.transform.Find("HeroTextList0").transform.Find("HeroText").GetComponent<Text>().text = "유저";
                m_TextBoxCollection.transform.Find("HeroTextList1").transform.Find("HeroText").GetComponent<Text>().text = "유저";
                m_TextBoxCollection.transform.Find("HeroTextList2").transform.Find("HeroText").GetComponent<Text>().text = "유저";

                break;
            }
        }
    }



    public void ShowWithTeamMateWinRate() // 팀원별 버튼 클릭
    {
        if (m_listInstanceFindingObj.Count != 0) //
        {
            foreach (var obj in m_listInstanceFindingObj)
                Destroy(obj);
            m_listInstanceFindingObj.Clear();


            if (FindingHeroName)
            {
                for (int q = 0; q < listHeroWL.Count; ++q)
                {
                    if (m_SearchInputBox.GetComponent<InputField>().text == listHeroWL[q].HeroName)
                    {
                        for (int i = 0; i < listHeroWL[q].listTeamMateHero.Count; ++i)
                        {
                            GameObject Instance = (GameObject)Instantiate(SampleHero, m_FindingRecordList.transform);

                            Instance.SetActive(true);

                            int matchcount = listHeroWL[q].listTeamMateHero[i].WinCount + listHeroWL[q].listTeamMateHero[i].LossCount;


                            float floatnum = (float)listHeroWL[q].listTeamMateHero[i].WinCount / (float)matchcount;
                            double rate = ((Math.Truncate(floatnum * 1000) / 1000) * 100);

                            Instance.transform.Find("HeroName").GetComponent<Text>().text = listHeroWL[q].listTeamMateHero[i].Name;
                            Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
                            Instance.transform.Find("Win").GetComponent<Text>().text = listHeroWL[q].listTeamMateHero[i].WinCount.ToString();
                            Instance.transform.Find("Loss").GetComponent<Text>().text = listHeroWL[q].listTeamMateHero[i].LossCount.ToString();
                            Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";
                            m_listInstanceFindingObj.Add(Instance);
                        }
                    }
                }
                m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = NowFindingName + "의 팀영웅별 승률";

            }

            if (FindingUserName)
            {
                for (int q = 0; q < listUserWL.Count; ++q)
                {
                    if (m_SearchInputBox.GetComponent<InputField>().text == listUserWL[q].UserName)
                    {
                        for (int i = 0; i < listUserWL[q].listTeamMate.Count; ++i)
                        {
                            GameObject Instance = (GameObject)Instantiate(SampleHero, m_FindingRecordList.transform);

                            Instance.SetActive(true);

                            int matchcount = listUserWL[q].listTeamMate[i].WinCount + listUserWL[q].listTeamMate[i].LossCount;


                            float floatnum = (float)listUserWL[q].listTeamMate[i].WinCount / (float)matchcount;
                            double rate = ((Math.Truncate(floatnum * 1000) / 1000) * 100);

                            Instance.transform.Find("HeroName").GetComponent<Text>().text = listUserWL[q].listTeamMate[i].Name;
                            Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
                            Instance.transform.Find("Win").GetComponent<Text>().text = listUserWL[q].listTeamMate[i].WinCount.ToString();
                            Instance.transform.Find("Loss").GetComponent<Text>().text = listUserWL[q].listTeamMate[i].LossCount.ToString();
                            Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";
                            m_listInstanceFindingObj.Add(Instance);
                        }
                    }
                }
                m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = NowFindingName + "의 팀원별 승률";

            }

        }
    }



    public void ShowWithEnemyMateWinRate() // 적팀원별 버튼 클릭
    {
        if (m_listInstanceFindingObj.Count != 0) //
        {
            foreach (var obj in m_listInstanceFindingObj)
                Destroy(obj);
            m_listInstanceFindingObj.Clear();



            if (FindingHeroName)
            {
                for (int q = 0; q < listHeroWL.Count; ++q)
                {
                    if (m_SearchInputBox.GetComponent<InputField>().text == listHeroWL[q].HeroName)
                    {
                        for (int i = 0; i < listHeroWL[q].listEnemyMateHero.Count; ++i)
                        {
                            GameObject Instance = (GameObject)Instantiate(SampleHero, m_FindingRecordList.transform);

                            Instance.SetActive(true);

                            int matchcount = listHeroWL[q].listEnemyMateHero[i].WinCount + listHeroWL[q].listEnemyMateHero[i].LossCount;


                            float floatnum = (float)listHeroWL[q].listEnemyMateHero[i].WinCount / (float)matchcount;
                            double rate = ((Math.Truncate(floatnum * 1000) / 1000) * 100);

                            Instance.transform.Find("HeroName").GetComponent<Text>().text = listHeroWL[q].listEnemyMateHero[i].Name;
                            Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
                            Instance.transform.Find("Win").GetComponent<Text>().text = listHeroWL[q].listEnemyMateHero[i].WinCount.ToString();
                            Instance.transform.Find("Loss").GetComponent<Text>().text = listHeroWL[q].listEnemyMateHero[i].LossCount.ToString();
                            Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";
                            m_listInstanceFindingObj.Add(Instance);
                        }
                    }
                }

                m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = NowFindingName + "의 적영웅상대별 승률";
            }




            if (FindingUserName)
            {
                for (int q = 0; q < listUserWL.Count; ++q)
                {
                    if (m_SearchInputBox.GetComponent<InputField>().text == listUserWL[q].UserName)
                    {
                        for (int i = 0; i < listUserWL[q].listEnemyMate.Count; ++i)
                        {
                            GameObject Instance = (GameObject)Instantiate(SampleHero, m_FindingRecordList.transform);

                            Instance.SetActive(true);

                            int matchcount = listUserWL[q].listEnemyMate[i].WinCount + listUserWL[q].listEnemyMate[i].LossCount;

                            float floatnum = (float)listUserWL[q].listEnemyMate[i].WinCount / (float)matchcount;
                            double rate = ((Math.Truncate(floatnum * 1000) / 1000) * 100);

                            Instance.transform.Find("HeroName").GetComponent<Text>().text = listUserWL[q].listEnemyMate[i].Name;
                            Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
                            Instance.transform.Find("Win").GetComponent<Text>().text = listUserWL[q].listEnemyMate[i].WinCount.ToString();
                            Instance.transform.Find("Loss").GetComponent<Text>().text = listUserWL[q].listEnemyMate[i].LossCount.ToString();
                            Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";
                            m_listInstanceFindingObj.Add(Instance);
                        }
                    }
                }

                m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = NowFindingName + "의 유저상대별 승률";

            }
        }
    }





    public void ClickHeroWinRateWithDate() // 날짜 별 검색 버튼
    {
        if (!m_LeftDateInputBox.activeSelf)
        {
            if (m_listInstanceDateObj.Count == 0)
            {
                m_LeftDateInputBox.SetActive(true);
                m_RightDateInputBox.SetActive(true);
                m_LeftDateInputBox.GetComponent<InputField>().Select();

            }
            else
            {
                ShowAllHeroRecord();

                foreach (var obj in m_listInstanceDateObj)
                {
                    Destroy(obj);
                }
                m_listInstanceDateObj.Clear();

            }
        }
        else
        {
            string StartDay = m_LeftDateInputBox.GetComponent<InputField>().text;
            string EndDay = m_RightDateInputBox.GetComponent<InputField>().text;

            bool bStartRecording = true;

            if (StartDay == "")
            {
                bStartRecording = false;
            }

            if (StartDay != "" && EndDay == "")
            {
                EndDay = StartDay; // 같은날짜로
            }


            int result = 0;
            for (int i = 0; i < StartDay.Length; i++) // 숫자아니면거르기
            {
                if (!int.TryParse(StartDay, out result))
                {
                    bStartRecording = false;
                }
            }
            for (int i = 0; i < EndDay.Length; i++)
            {
                if (!int.TryParse(EndDay, out result))
                {
                    bStartRecording = false;
                }
            }
            if (!bStartRecording)
            {
                m_LeftDateInputBox.SetActive(false);
                m_RightDateInputBox.SetActive(false);
                return;
            }
            int iday1 = int.Parse(StartDay);
            int iday2 = int.Parse(EndDay);

            //listDateToDateRecord클리어
            listDateToDateRecord.Clear();

            

            //승률정렬?
            float awinrate = 0;
            float bwinrate = 0;
            int aheromatchcount = 0;
            int bheromatchcount = 0;


            #region 날짜별 전체 영웅 전적            
            if (m_listInstanceHeroObj.Count != 0)
            {
                if (iday2 >= iday1)
                {
                    bool bone = false;
                    bool btwo = false;

                    foreach (var obj in m_listInstanceHeroObj)
                    {
                        Destroy(obj);
                    }
                    m_listInstanceHeroObj.Clear();

                    foreach (var hwl in listHeroWL)
                    {
                        hwl.DateToDateRecord.Name = ""; // 초기화
                        foreach (var hereplaydate_ in hwl.listDate)
                        {
                            if (hereplaydate_.intDate == iday1)
                            {
                                bone = true;
                            }

                            if (hereplaydate_.intDate == iday2)
                            {
                                btwo = true;
                            }
                        }
                    }

                    int datetodateAllMatchCount = 0; // 다듬어야함   
                    int datetodateLeftWinCount = 0;
                    int datetodateRighWinCount = 0;
                    if (bone && btwo) // 날짜 둘다 존재 할때
                    {

                        foreach (var hwl in listHeroWL)
                        {
                            foreach (var LD_ in hwl.listDate)
                            {
                                if (iday1 <= LD_.intDate && LD_.intDate <= iday2)
                                {

                                    hwl.DateToDateRecord.Name = hwl.HeroName;
                                    hwl.DateToDateRecord.StartDate = StartDay;
                                    hwl.DateToDateRecord.EndDate = EndDay;
                                    hwl.DateToDateRecord.WinCount += LD_.WinCount;
                                    hwl.DateToDateRecord.LossCount += LD_.LossCount;
                                    datetodateAllMatchCount += LD_.WinCount + LD_.LossCount;
                                    datetodateLeftWinCount += LD_.DateLeftWinCount;
                                    datetodateRighWinCount += LD_.DateRightWinCount;
                                }
                            }

                            if (hwl.DateToDateRecord.Name != "") // 만들어져있다면
                            {
                                listDateToDateRecord.Add(hwl.DateToDateRecord);

                                //GameObject Instance = (GameObject)Instantiate(SampleHero, m_DateHeroList.transform);

                                //Instance.SetActive(true);

                                //int matchcount = hwl.DateToDateRecord.WinCount + hwl.DateToDateRecord.LossCount;
                         
                                //float floatnum = (float)hwl.DateToDateRecord.WinCount / (float)matchcount;
                                //double rate = ((Math.Truncate(floatnum * 1000) / 1000) * 100);

                                //Instance.transform.Find("HeroName").GetComponent<Text>().text = hwl.DateToDateRecord.Name;
                                //Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
                                //Instance.transform.Find("Win").GetComponent<Text>().text = hwl.DateToDateRecord.WinCount.ToString();
                                //Instance.transform.Find("Loss").GetComponent<Text>().text = hwl.DateToDateRecord.LossCount.ToString();
                                //Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";

                                //m_listInstanceDateObj.Add(Instance);                                
                            }

                       
                        }
                        datetodateAllMatchCount = datetodateAllMatchCount / 6;
                        datetodateLeftWinCount = datetodateLeftWinCount / 3;
                        datetodateRighWinCount = datetodateRighWinCount / 3;

                        float lwr = (float)datetodateLeftWinCount / (float)datetodateAllMatchCount;
                        double lrate = ((Math.Truncate(lwr * 1000) / 1000) * 100);

                        float rwr = (float)datetodateRighWinCount / (float)datetodateAllMatchCount;
                        double rrate = ((Math.Truncate(rwr * 1000) / 1000) * 100);

                        m_AllMatchCount.GetComponent<Text>().text = datetodateAllMatchCount.ToString();
                        m_LeftWinRate.GetComponent<Text>().text = lrate.ToString() + "%";
                        m_RightWinRate.GetComponent<Text>().text = rrate.ToString() + "%";



                        m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = "기간 내 영웅 전적";


                        //m_LeftDateInputBox.GetComponent<InputField>().text = "";
                        //m_RightDateInputBox.GetComponent<InputField>().text = "";

                        m_LeftDateText.GetComponent<Text>().text = StartDay;
                        m_RightDateText.GetComponent<Text>().text = EndDay;

                        m_TextBoxCollection.transform.Find("HeroTextList0").transform.Find("HeroText").GetComponent<Text>().text = "유저";
                        m_TextBoxCollection.transform.Find("HeroTextList1").transform.Find("HeroText").GetComponent<Text>().text = "유저";
                        m_TextBoxCollection.transform.Find("HeroTextList2").transform.Find("HeroText").GetComponent<Text>().text = "유저";
                    }
                }
            }
            #endregion

            #region 날짜별 전체 유저 전적
            if (m_listInstanceUserObj.Count != 0)
            {
                if (iday2 >= iday1)
                {
                    bool bone = false;
                    bool btwo = false;

                    foreach (var obj in m_listInstanceUserObj)
                        Destroy(obj);
                    m_listInstanceUserObj.Clear();

                    foreach (var hwl in listUserWL)
                    {
                        hwl.DateToDateRecord.Name = ""; // 초기화
                        foreach (var hereplaydate_ in hwl.listDate)
                        {
                            if (hereplaydate_.intDate == iday1)
                            {
                                bone = true;
                            }

                            if (hereplaydate_.intDate == iday2)
                            {
                                btwo = true;
                            }
                        }

                    }

                    int datetodateAllMatchCount = 0; // 다듬어야함   
                    int datetodateLeftWinCount = 0;
                    int datetodateRighWinCount = 0;
                    if (bone && btwo) // 날짜 둘다 존재 할때
                    {

                        foreach (var hwl in listUserWL)
                        {
                            foreach (var heroplaydate2_ in hwl.listDate)
                            {
                                if (iday1 <= heroplaydate2_.intDate && heroplaydate2_.intDate <= iday2)
                                {

                                    hwl.DateToDateRecord.Name = hwl.UserName;
                                    hwl.DateToDateRecord.StartDate = StartDay;
                                    hwl.DateToDateRecord.EndDate = EndDay;
                                    hwl.DateToDateRecord.WinCount += heroplaydate2_.WinCount;
                                    hwl.DateToDateRecord.LossCount += heroplaydate2_.LossCount;
                                    datetodateAllMatchCount += heroplaydate2_.WinCount + heroplaydate2_.LossCount;
                                    datetodateLeftWinCount += heroplaydate2_.DateLeftWinCount;
                                    datetodateRighWinCount += heroplaydate2_.DateRightWinCount;
                                }
                            }

                            if (hwl.DateToDateRecord.Name != "") // 만들어져있다면
                            {
                                listDateToDateRecord.Add(hwl.DateToDateRecord);
                                //GameObject Instance = (GameObject)Instantiate(SampleHero, m_DateHeroList.transform);

                                //Instance.SetActive(true);

                                //int matchcount = hwl.DateToDateRecord.WinCount + hwl.DateToDateRecord.LossCount;
                                ////그날의 경기횟수 = datetodateAllMatchCount / 6;

                                //float floatnum = (float)hwl.DateToDateRecord.WinCount / (float)matchcount;
                                //double rate = ((Math.Truncate(floatnum * 1000) / 1000) * 100);

                                //Instance.transform.Find("HeroName").GetComponent<Text>().text = hwl.DateToDateRecord.Name;
                                //Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
                                //Instance.transform.Find("Win").GetComponent<Text>().text = hwl.DateToDateRecord.WinCount.ToString();
                                //Instance.transform.Find("Loss").GetComponent<Text>().text = hwl.DateToDateRecord.LossCount.ToString();
                                //Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";
                                //m_listInstanceDateObj.Add(Instance);

                            }
                        }
                        datetodateAllMatchCount = datetodateAllMatchCount / 6;
                        datetodateLeftWinCount = datetodateLeftWinCount / 3;
                        datetodateRighWinCount = datetodateRighWinCount / 3;

                        float lwr = (float)datetodateLeftWinCount / (float)datetodateAllMatchCount;
                        double lrate = ((Math.Truncate(lwr * 1000) / 1000) * 100);

                        float rwr = (float)datetodateRighWinCount / (float)datetodateAllMatchCount;
                        double rrate = ((Math.Truncate(rwr * 1000) / 1000) * 100);

                        m_AllMatchCount.GetComponent<Text>().text = datetodateAllMatchCount.ToString();
                        m_LeftWinRate.GetComponent<Text>().text = lrate.ToString() + "%";
                        m_RightWinRate.GetComponent<Text>().text = rrate.ToString() + "%";





                        m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = "기간 내 유저 전적";



                        //m_LeftDateInputBox.GetComponent<InputField>().text = "";
                        //m_RightDateInputBox.GetComponent<InputField>().text = "";

                        m_LeftDateText.GetComponent<Text>().text = StartDay;
                        m_RightDateText.GetComponent<Text>().text = EndDay;

                        m_TextBoxCollection.transform.Find("HeroTextList0").transform.Find("HeroText").GetComponent<Text>().text = "영웅";
                        m_TextBoxCollection.transform.Find("HeroTextList1").transform.Find("HeroText").GetComponent<Text>().text = "영웅";
                        m_TextBoxCollection.transform.Find("HeroTextList2").transform.Find("HeroText").GetComponent<Text>().text = "영웅";
                    }
                }
            }
            #endregion

            

            if (m_listInstanceFindingObj.Count != 0)
            {
                //두개다!

                if (iday2 >= iday1)
                {
                    bool bone = false;
                    bool btwo = false;

                    foreach (var obj in m_listInstanceFindingObj)
                        Destroy(obj);
                    m_listInstanceFindingObj.Clear();

                    #region 날짜별 특정 영웅의 유저 전적
                    if (FindingHeroName)
                    {


                        foreach (var hwl in listHeroWL)
                        {
                            hwl.DateToDateRecord.Name = ""; // 초기화
                            foreach (var userplaydate_ in hwl.listDate)
                            {
                                if (userplaydate_.intDate == iday1)
                                {
                                    bone = true;
                                }

                                if (userplaydate_.intDate == iday2)
                                {
                                    btwo = true;
                                }
                            }

                        }

                        int datetodateAllMatchCount = 0; // 다듬어야함   
                        int datetodateLeftWinCount = 0;
                        int datetodateRighWinCount = 0;

                        int datetodateAllWinCount = 0;
                        int datetodateAllLossCount = 0;

                        if (bone && btwo) // 날짜 둘다 존재 할때
                        {
                            foreach (var hwl in listHeroWL) // 유저
                            {
                                if (m_SearchInputBox.GetComponent<InputField>().text == hwl.HeroName) // 검색했던 영웅
                                {
                                    foreach (var playedhwl in hwl.listUserPlayingThisHero) // 영웅을 플레이 한 유저들의 리스트
                                    {
                                        foreach (var LD_ in playedhwl.listDate)
                                        {
                                            if (iday1 <= LD_.intDate && LD_.intDate <= iday2)
                                            {
                                                playedhwl.DateToDateRecord.Name = playedhwl.UserName;
                                                playedhwl.DateToDateRecord.StartDate = StartDay;
                                                playedhwl.DateToDateRecord.EndDate = EndDay;
                                                playedhwl.DateToDateRecord.WinCount += LD_.WinCount;
                                                playedhwl.DateToDateRecord.LossCount += LD_.LossCount;
                                                datetodateAllMatchCount += LD_.WinCount + LD_.LossCount;
                                                datetodateLeftWinCount += LD_.DateLeftWinCount;
                                                datetodateRighWinCount += LD_.DateRightWinCount;
                                                datetodateAllWinCount += LD_.WinCount;
                                                datetodateAllLossCount += LD_.LossCount;
                                            }
                                        }

                                        if (playedhwl.DateToDateRecord.Name != "" && playedhwl.DateToDateRecord.Name != null) // 만들어져있다면
                                        {
                                            listDateToDateRecord.Add(playedhwl.DateToDateRecord);
                                            //GameObject Instance = (GameObject)Instantiate(SampleHero, m_DateHeroList.transform);

                                            //Instance.SetActive(true);

                                            //int matchcount = playedhwl.DateToDateRecord.WinCount + playedhwl.DateToDateRecord.LossCount;

                                            //float floatnum = (float)playedhwl.DateToDateRecord.WinCount / (float)matchcount;
                                            //double rate = ((Math.Truncate(floatnum * 1000) / 1000) * 100);

                                            //Instance.transform.Find("HeroName").GetComponent<Text>().text = playedhwl.DateToDateRecord.Name;
                                            //Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
                                            //Instance.transform.Find("Win").GetComponent<Text>().text = playedhwl.DateToDateRecord.WinCount.ToString();
                                            //Instance.transform.Find("Loss").GetComponent<Text>().text = playedhwl.DateToDateRecord.LossCount.ToString();
                                            //Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";
                                            //m_listInstanceDateObj.Add(Instance);

                                        }
                                    }
                                }
                            }
                     

                            float lwr = (float)datetodateLeftWinCount / (float)datetodateAllMatchCount;
                            double lrate = ((Math.Truncate(lwr * 1000) / 1000) * 100);

                            float rwr = (float)datetodateRighWinCount / (float)datetodateAllMatchCount;
                            double rrate = ((Math.Truncate(rwr * 1000) / 1000) * 100);

                            float awr = (float)datetodateAllWinCount / (float)datetodateAllMatchCount;
                            double awrate = ((Math.Truncate(awr * 1000) / 1000) * 100);
                                                        
                            m_AllMatchCount.GetComponent<Text>().text = datetodateAllMatchCount.ToString();
                            m_LeftWinRate.GetComponent<Text>().text = datetodateAllWinCount.ToString();
                            m_RightWinRate.GetComponent<Text>().text = datetodateAllLossCount.ToString();
                            m_UserWinRate.GetComponent<Text>().text = awrate.ToString() + "%";

                            m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = m_SearchInputBox.GetComponent<InputField>().text;
                            m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = NowFindingName + "의 기간 내 유저 전적";
                            m_LeftDateInputBox.GetComponent<InputField>().text = "";
                            m_RightDateInputBox.GetComponent<InputField>().text = "";

                            m_LeftDateText.GetComponent<Text>().text = StartDay;
                            m_RightDateText.GetComponent<Text>().text = EndDay;



                            m_TextBoxCollection.transform.Find("HeroTextList0").transform.Find("HeroText").GetComponent<Text>().text = "유저";
                            m_TextBoxCollection.transform.Find("HeroTextList1").transform.Find("HeroText").GetComponent<Text>().text = "유저";
                            m_TextBoxCollection.transform.Find("HeroTextList2").transform.Find("HeroText").GetComponent<Text>().text = "유저";

                        }

                    }
                    #endregion

                    #region 날짜별 특정 유저의 영웅 전적

                    if (FindingUserName)
                    {


                        foreach (var uwl in listUserWL)
                        {
                            uwl.DateToDateRecord.Name = ""; // 초기화
                            foreach (var hereplaydate_ in uwl.listDate)
                            {
                                if (hereplaydate_.intDate == iday1)
                                {
                                    bone = true;
                                }

                                if (hereplaydate_.intDate == iday2)
                                {
                                    btwo = true;
                                }
                            }

                        }

                        int datetodateAllMatchCount = 0; // 다듬어야함   
                        int datetodateLeftWinCount = 0;
                        int datetodateRighWinCount = 0;

                        int datetodateAllWinCount = 0;
                        int datetodateAllLossCount = 0;

                        if (bone && btwo) // 날짜 둘다 존재 할때
                        {
                            foreach (var hwl in listUserWL) // 유저
                            {
                                if (m_SearchInputBox.GetComponent<InputField>().text == hwl.UserName) // 검색했던 유저
                                {
                                    foreach (var playedhwl in hwl.listHeroPlayedbyUser) // 유저가 플레이했던 영웅들의 리스트
                                    {
                                        foreach (var LD_ in playedhwl.listDate)
                                        {
                                            if (iday1 <= LD_.intDate && LD_.intDate <= iday2)
                                            {
                                                playedhwl.DateToDateRecord.Name = playedhwl.HeroName;
                                                playedhwl.DateToDateRecord.StartDate = StartDay;
                                                playedhwl.DateToDateRecord.EndDate = EndDay;
                                                playedhwl.DateToDateRecord.WinCount += LD_.WinCount;
                                                playedhwl.DateToDateRecord.LossCount += LD_.LossCount;
                                                datetodateAllMatchCount += LD_.WinCount + LD_.LossCount;
                                                datetodateLeftWinCount += LD_.DateLeftWinCount;
                                                datetodateRighWinCount += LD_.DateRightWinCount;
                                                datetodateAllWinCount += LD_.WinCount;
                                                datetodateAllLossCount += LD_.LossCount;
                                            }
                                        }

                                        if (playedhwl.DateToDateRecord.Name != "" && playedhwl.DateToDateRecord.Name != null) // 만들어져있다면
                                        {
                                            listDateToDateRecord.Add(playedhwl.DateToDateRecord);
                                            //GameObject Instance = (GameObject)Instantiate(SampleHero, m_DateHeroList.transform);

                                            //Instance.SetActive(true);

                                            //int matchcount = playedhwl.DateToDateRecord.WinCount + playedhwl.DateToDateRecord.LossCount;

                                            //float floatnum = (float)playedhwl.DateToDateRecord.WinCount / (float)matchcount;
                                            //double rate = ((Math.Truncate(floatnum * 1000) / 1000) * 100);

                                            //Instance.transform.Find("HeroName").GetComponent<Text>().text = playedhwl.DateToDateRecord.Name;
                                            //Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
                                            //Instance.transform.Find("Win").GetComponent<Text>().text = playedhwl.DateToDateRecord.WinCount.ToString();
                                            //Instance.transform.Find("Loss").GetComponent<Text>().text = playedhwl.DateToDateRecord.LossCount.ToString();
                                            //Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";
                                            //m_listInstanceDateObj.Add(Instance);

                                        }
                                    }
                                }
                            }
                            //datetodateAllMatchCount = datetodateAllMatchCount / 6;
                            //datetodateLeftWinCount = datetodateLeftWinCount / 3;
                            //datetodateRighWinCount = datetodateRighWinCount / 3;

                            float lwr = (float)datetodateLeftWinCount / (float)datetodateAllMatchCount;
                            double lrate = ((Math.Truncate(lwr * 1000) / 1000) * 100);

                            float rwr = (float)datetodateRighWinCount / (float)datetodateAllMatchCount;
                            double rrate = ((Math.Truncate(rwr * 1000) / 1000) * 100);

                            m_AllMatchCount.GetComponent<Text>().text = datetodateAllMatchCount.ToString();
                            m_LeftWinRate.GetComponent<Text>().text = datetodateAllWinCount.ToString();
                            m_RightWinRate.GetComponent<Text>().text = datetodateAllLossCount.ToString();


                            m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = m_SearchInputBox.GetComponent<InputField>().text;
                            m_TextBoxCollection.transform.Find("FindingNameText").GetComponent<Text>().text = NowFindingName + "의 기간 내 영웅 전적";
                            m_LeftDateInputBox.GetComponent<InputField>().text = "";
                            m_RightDateInputBox.GetComponent<InputField>().text = "";

                            m_LeftDateText.GetComponent<Text>().text = StartDay;
                            m_RightDateText.GetComponent<Text>().text = EndDay;



                            m_TextBoxCollection.transform.Find("HeroTextList0").transform.Find("HeroText").GetComponent<Text>().text = "영웅";
                            m_TextBoxCollection.transform.Find("HeroTextList1").transform.Find("HeroText").GetComponent<Text>().text = "영웅";
                            m_TextBoxCollection.transform.Find("HeroTextList2").transform.Find("HeroText").GetComponent<Text>().text = "영웅";

                        }

                    }
                    #endregion

                }

            }



            listDateToDateRecord.Sort(
        delegate (DateToDateWL a, DateToDateWL b)
        {
            aheromatchcount = a.WinCount + a.LossCount;
            bheromatchcount = b.WinCount + b.LossCount;
            awinrate = (float)a.WinCount / (float)aheromatchcount;
            bwinrate = (float)b.WinCount / (float)bheromatchcount;

            if (awinrate < bwinrate) return 1;
            else if (awinrate > bwinrate) return -1;
            return 0;
        });

            foreach (var LDTDR in listDateToDateRecord)
            {
                GameObject Instance = (GameObject)Instantiate(SampleHero, m_DateHeroList.transform);

                Instance.SetActive(true);

                int matchcount = LDTDR.WinCount + LDTDR.LossCount;

                float floatnum = (float)LDTDR.WinCount / (float)matchcount;
                double rate = ((Math.Truncate(floatnum * 1000) / 1000) * 100);

                Instance.transform.Find("HeroName").GetComponent<Text>().text = LDTDR.Name;
                Instance.transform.Find("Match").GetComponent<Text>().text = matchcount.ToString();
                Instance.transform.Find("Win").GetComponent<Text>().text = LDTDR.WinCount.ToString();
                Instance.transform.Find("Loss").GetComponent<Text>().text = LDTDR.LossCount.ToString();
                Instance.transform.Find("WinRate").GetComponent<Text>().text = rate.ToString() + "%";

                m_listInstanceDateObj.Add(Instance);

            }



            m_LeftDateInputBox.SetActive(false);
            m_RightDateInputBox.SetActive(false);

        }
    }





    private void RegisterTeamMateHero(string TeamMate1_, string TeamMate2_, HeroWL HW_, int SplitIndex_, string[] splittext_)
    {
        bool bTeamMateHeroOverlapCheck11 = false; // 중복팀메이트 거르기
        bool bTeamMateHeroOverlapCheck22 = false; // 중복팀메이트 거르기


        for (int tt = 0; tt < HW_.listTeamMateHero.Count; ++tt)
        {
            if (TeamMate1_ == HW_.listTeamMateHero[tt].Name)
            {
                bTeamMateHeroOverlapCheck11 = true;
            }

            if (TeamMate2_ == HW_.listTeamMateHero[tt].Name)
            {
                bTeamMateHeroOverlapCheck22 = true;
            }
        }

        if (!bTeamMateHeroOverlapCheck11)
        {
            MateWL imsiteammatewl = new MateWL();
            imsiteammatewl.Name = TeamMate1_;
            imsiteammatewl.WinCount = 0;
            imsiteammatewl.LossCount = 0;
            HW_.listTeamMateHero.Add(imsiteammatewl);
        }

        if (!bTeamMateHeroOverlapCheck22)
        {
            MateWL imsiteammatewl = new MateWL();
            imsiteammatewl.Name = TeamMate2_;
            imsiteammatewl.WinCount = 0;
            imsiteammatewl.LossCount = 0;
            HW_.listTeamMateHero.Add(imsiteammatewl);
        }

    }

    private void RegisterEnemyMateHero(string EnemyMate1_, string EnemyMate2_, string EnemyMate3_, HeroWL HW_, int SplitIndex_, string[] splittext_)
    {
        bool bEnemyMateHeroOverlapCheck11 = false; // 중복적팀원 거르기
        bool bEnemyMateHeroOverlapCheck22 = false; // 중복적팀원 거르기
        bool bEnemyMateHeroOverlapCheck33 = false; // 중복적팀원 거르기

        for (int tt = 0; tt < HW_.listEnemyMateHero.Count; ++tt)
        {
            if (EnemyMate1_ == HW_.listEnemyMateHero[tt].Name)
            {
                bEnemyMateHeroOverlapCheck11 = true;
            }

            if (EnemyMate2_ == HW_.listEnemyMateHero[tt].Name)
            {
                bEnemyMateHeroOverlapCheck22 = true;
            }

            if (EnemyMate3_ == HW_.listEnemyMateHero[tt].Name)
            {
                bEnemyMateHeroOverlapCheck33 = true;
            }
        }

        if (!bEnemyMateHeroOverlapCheck11)
        {
            MateWL imsiteammatewl = new MateWL();
            imsiteammatewl.Name = EnemyMate1_;
            imsiteammatewl.WinCount = 0;
            imsiteammatewl.LossCount = 0;
            HW_.listEnemyMateHero.Add(imsiteammatewl);
        }

        if (!bEnemyMateHeroOverlapCheck22)
        {
            MateWL imsiteammatewl = new MateWL();
            imsiteammatewl.Name = EnemyMate2_;
            imsiteammatewl.WinCount = 0;
            imsiteammatewl.LossCount = 0;
            HW_.listEnemyMateHero.Add(imsiteammatewl);
        }

        if (!bEnemyMateHeroOverlapCheck33)
        {
            MateWL imsiteammatewl = new MateWL();
            imsiteammatewl.Name = EnemyMate3_;
            imsiteammatewl.WinCount = 0;
            imsiteammatewl.LossCount = 0;
            HW_.listEnemyMateHero.Add(imsiteammatewl);
        }

    }

    private void RegisterTeamMateHeroWLCount(string TeamMate1_, string TeamMate2_, List<MateWL> ListTeamMateHeroWL_, bool winloss)
    {
        foreach (var LTM in ListTeamMateHeroWL_)
        {
            if (winloss)
            {
                if (LTM.Name == TeamMate1_)
                    LTM.WinCount++;
                if (LTM.Name == TeamMate2_)
                    LTM.WinCount++;
            }
            else
            {
                if (LTM.Name == TeamMate1_)
                    LTM.LossCount++;
                if (LTM.Name == TeamMate2_)
                    LTM.LossCount++;
            }

        }

    }

    private void RegisterEnemyMateHeroWLCount(string EnemyMate1_, string EnemyMate2_, string EnemyMate3_, List<MateWL> ListEnemyMateHeroWL_, bool winloss)
    {
        foreach (var LEM in ListEnemyMateHeroWL_)
        {
            if (winloss)
            {
                if (LEM.Name == EnemyMate1_)
                    LEM.WinCount++;
                if (LEM.Name == EnemyMate2_)
                    LEM.WinCount++;
                if (LEM.Name == EnemyMate3_)
                    LEM.WinCount++;
            }
            else
            {
                if (LEM.Name == EnemyMate1_)
                    LEM.LossCount++;
                if (LEM.Name == EnemyMate2_)
                    LEM.LossCount++;
                if (LEM.Name == EnemyMate3_)
                    LEM.LossCount++;
            }

        }

    }




    private void RegisterTeamMate(string TeamMate1_, string TeamMate2_, UserWL UW_, int SplitIndex_, string[] splittext_)
    {
        bool bTeamMateOverlapCheck11 = false; // 중복팀메이트 거르기
        bool bTeamMateOverlapCheck22 = false; // 중복팀메이트 거르기


        for (int tt = 0; tt < UW_.listTeamMate.Count; ++tt)
        {
            if (TeamMate1_ == UW_.listTeamMate[tt].Name)
            {
                bTeamMateOverlapCheck11 = true;
            }

            if (TeamMate2_ == UW_.listTeamMate[tt].Name)
            {
                bTeamMateOverlapCheck22 = true;
            }
        }

        if (!bTeamMateOverlapCheck11)
        {
            MateWL imsiteammatewl = new MateWL();
            imsiteammatewl.Name = TeamMate1_;
            imsiteammatewl.WinCount = 0;
            imsiteammatewl.LossCount = 0;
            UW_.listTeamMate.Add(imsiteammatewl);
        }

        if (!bTeamMateOverlapCheck22)
        {
            MateWL imsiteammatewl = new MateWL();
            imsiteammatewl.Name = TeamMate2_;
            imsiteammatewl.WinCount = 0;
            imsiteammatewl.LossCount = 0;
            UW_.listTeamMate.Add(imsiteammatewl);
        }

    }

    private void RegisterEnemyMate(string EnemyMate1_, string EnemyMate2_, string EnemyMate3_, UserWL UW_, int SplitIndex_, string[] splittext_)
    {


        bool bEnemyMateOverlapCheck11 = false; // 중복적팀원 거르기
        bool bEnemyMateOverlapCheck22 = false; // 중복적팀원 거르기
        bool bEnemyMateOverlapCheck33 = false; // 중복적팀원 거르기

        for (int tt = 0; tt < UW_.listEnemyMate.Count; ++tt)
        {
            if (EnemyMate1_ == UW_.listEnemyMate[tt].Name)
            {
                bEnemyMateOverlapCheck11 = true;
            }

            if (EnemyMate2_ == UW_.listEnemyMate[tt].Name)
            {
                bEnemyMateOverlapCheck22 = true;
            }

            if (EnemyMate3_ == UW_.listEnemyMate[tt].Name)
            {
                bEnemyMateOverlapCheck33 = true;
            }
        }

        if (!bEnemyMateOverlapCheck11)
        {
            MateWL imsiteammatewl = new MateWL();
            imsiteammatewl.Name = EnemyMate1_;
            imsiteammatewl.WinCount = 0;
            imsiteammatewl.LossCount = 0;
            UW_.listEnemyMate.Add(imsiteammatewl);
        }

        if (!bEnemyMateOverlapCheck22)
        {
            MateWL imsiteammatewl = new MateWL();
            imsiteammatewl.Name = EnemyMate2_;
            imsiteammatewl.WinCount = 0;
            imsiteammatewl.LossCount = 0;
            UW_.listEnemyMate.Add(imsiteammatewl);
        }

        if (!bEnemyMateOverlapCheck33)
        {
            MateWL imsiteammatewl = new MateWL();
            imsiteammatewl.Name = EnemyMate3_;
            imsiteammatewl.WinCount = 0;
            imsiteammatewl.LossCount = 0;
            UW_.listEnemyMate.Add(imsiteammatewl);
        }

    }

    private void RegisterTeamMateWLCount(string TeamMate1_, string TeamMate2_, List<MateWL> ListTeamMateWL_, bool winloss)
    {
        foreach (var LTM in ListTeamMateWL_)
        {
            if (winloss)
            {
                if (LTM.Name == TeamMate1_)
                    LTM.WinCount++;
                if (LTM.Name == TeamMate2_)
                    LTM.WinCount++;
            }
            else
            {
                if (LTM.Name == TeamMate1_)
                    LTM.LossCount++;
                if (LTM.Name == TeamMate2_)
                    LTM.LossCount++;
            }

        }

    }

    private void RegisterEnemyMateWLCount(string EnemyMate1_, string EnemyMate2_, string EnemyMate3_, List<MateWL> ListEnemyMateWL_, bool winloss)
    {
        foreach (var LEM in ListEnemyMateWL_)
        {
            if (winloss)
            {
                if (LEM.Name == EnemyMate1_)
                    LEM.WinCount++;
                if (LEM.Name == EnemyMate2_)
                    LEM.WinCount++;
                if (LEM.Name == EnemyMate3_)
                    LEM.WinCount++;
            }
            else
            {
                if (LEM.Name == EnemyMate1_)
                    LEM.LossCount++;
                if (LEM.Name == EnemyMate2_)
                    LEM.LossCount++;
                if (LEM.Name == EnemyMate3_)
                    LEM.LossCount++;
            }

        }

    }



    private void AddlistHeroWLDateWinLoss(HeroWL HW, bool winloss, string datetext, string RL)
    {
        for (int dd = 0; dd < HW.listDate.Count; ++dd)
        {
            if (HW.listDate[dd].strDate == datetext)
            {
                if (!winloss)
                {
                    HW.listDate[dd].LossCount++;

                }
                else
                {
                    HW.listDate[dd].WinCount++;
                    if (RL == "오")
                        HW.listDate[dd].DateRightWinCount++;
                    else
                        HW.listDate[dd].DateLeftWinCount++;

                }

                break;
            }
        }
    }

    private void AddlistUserWLDateWinLoss(UserWL UW, bool winloss, string datetext, string RL)
    {
        for (int dd = 0; dd < UW.listDate.Count; ++dd)
        {
            if (UW.listDate[dd].strDate == datetext)
            {
                if (!winloss)
                    UW.listDate[dd].LossCount++;
                else
                {
                    UW.listDate[dd].WinCount++;
                    if (RL == "오")
                        UW.listDate[dd].DateRightWinCount++;
                    else
                        UW.listDate[dd].DateLeftWinCount++;
                }
                break;
            }
        }
    }

    private void AddHeroDateList(HeroWL HWL, string datetext)
    {
        bool bDateOverlapCheck = false; // 중복날짜 거르기

        for (int d = 0; d < HWL.listDate.Count; ++d)
        {
            if (HWL.listDate[d].strDate == datetext)
            {
                bDateOverlapCheck = true;
                break;
            }
        }

        if (!bDateOverlapCheck) // 미등록된 날짜등록
        {
            DateWL imsidatewl = new DateWL();
            imsidatewl.strDate = datetext;
            imsidatewl.intDate = int.Parse(datetext);
            imsidatewl.WinCount = 0;
            imsidatewl.LossCount = 0;
            HWL.listDate.Add(imsidatewl);
        }
    }

    private void AddUserDateList(UserWL UWL, string datetext) // 플레이한 날짜 등록
    {
        bool bDateOverlapCheck = false; // 중복날짜 거르기

        for (int d = 0; d < UWL.listDate.Count; ++d)
        {
            if (UWL.listDate[d].strDate == datetext)
            {
                bDateOverlapCheck = true;
                break;
            }
        }

        if (!bDateOverlapCheck) // 미등록된 날짜등록
        {
            DateWL imsidatewl = new DateWL();
            imsidatewl.strDate = datetext;
            imsidatewl.intDate = int.Parse(datetext);
            imsidatewl.WinCount = 0;
            imsidatewl.LossCount = 0;
            UWL.listDate.Add(imsidatewl);
        }
    }


}
