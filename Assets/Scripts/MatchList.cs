﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

class MatchExample
{
    public string[] HeroName = null;
    public string[] UserName = null;
    public string Result = null;
    public string Date = null;
}

public class MatchList : MonoBehaviour
{
    static public MatchList Instance { private set; get; }

    public GameObject m_Input = null;
    public GameObject m_MatchListOBJ = null;
    public GameObject m_OneMatchSample = null;
    public Transform m_MatchList = null;
    private int iMatchCount = 1;

    List<MatchExample> listMatch = new List<MatchExample>();

    public GameObject m_AllMatchCount;
    public GameObject m_LeftWinRate;
    public GameObject m_RightWinRate;
    private int iLeftWin = 0;
    private int iRightWin = 0;

    private void Awake()
    {
       Instance = this;
    }
        // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CreateMatchList()
    {
        if(m_Input.activeSelf)
        {
            m_Input.SetActive(false);
            m_MatchListOBJ.SetActive(true);
        }
        else
        {
            m_Input.SetActive(true);
            m_MatchListOBJ.SetActive(false);
            return;
        }

        listMatch.Clear();
        LoadData();
        for (int i = 0; i < iMatchCount; ++i)
        {
            GameObject Instance = (GameObject)Instantiate(m_OneMatchSample, m_MatchList);

            Instance.SetActive(true);   

            for(int q = 0; q < 6; ++q)
            {                
                Instance.transform.Find(string.Format("HeroName{0}", q)).GetComponent<Text>().text = listMatch[i].HeroName[q];
                Instance.transform.Find(string.Format("UserName{0}", q)).GetComponent<Text>().text = listMatch[i].UserName[q];

            }

            Instance.transform.Find("Date").GetComponent<Text>().text = listMatch[i].Date;
            Instance.transform.Find("Result").GetComponent<Text>().text = listMatch[i].Result;

        }

        float lwr = (float)iLeftWin / (float)iMatchCount;
        double lrate = ((Math.Truncate(lwr * 1000) / 1000) * 100);

        float rwr = (float)iRightWin / (float)iMatchCount;
        double rrate = ((Math.Truncate(rwr * 1000) / 1000) * 100);

        m_AllMatchCount.GetComponent<Text>().text = iMatchCount.ToString();
        m_LeftWinRate.GetComponent<Text>().text = lrate.ToString() + "%";
        m_RightWinRate.GetComponent<Text>().text = rrate.ToString() + "%";
    }

    private void LoadData()
    {
        iMatchCount = 1; //초기화

        StreamReader sr = new StreamReader(Application.dataPath + "/StreamingAssets" + "/" + "RecordFile.txt");

        string source = sr.ReadLine();
        //경기당 14개 0 2 4 / 6 8 10 영웅
        //1 3 5 / 7 9 11 유저

        string[] sptext = source.Split(',');
        int numcount = 0;

        MatchExample mytemp = new MatchExample();
        for (int i = 0; i < sptext.Length; i++)
        {
            if(numcount == 0)
            {
                mytemp = new MatchExample();
                mytemp.HeroName = new string[6];
                mytemp.UserName = new string[6];
            }

            if (numcount < 12)
            {
                if (i % 2 == 0) // 영웅만
                {                   
                   mytemp.HeroName[numcount / 2] = sptext[i]; // 0,2,4,6,8,10 
                                                          // 0 1 2 3 4 5
                }

                if (i % 2 == 1) // 유저만
                {
                    int abc = numcount / 2;
                    mytemp.UserName[numcount / 2] = sptext[i]; //1,3,5,7,9,11
                }
                
            }

            numcount++;
            if (numcount % 14 == 0)
            {
                mytemp.Result = sptext[iMatchCount * 14 - 2];
                mytemp.Date = sptext[iMatchCount * 14 - 1];
                listMatch.Add(mytemp);

                if (mytemp.Result == "오")
                {
                    iRightWin++;

                }
                else
                {
                    iLeftWin++;

                }

                numcount = 0; // 1경기끝
                iMatchCount++;

            }

        }
        iMatchCount--;

    }
}
